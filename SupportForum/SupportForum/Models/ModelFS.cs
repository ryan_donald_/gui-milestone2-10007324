namespace SupportForum.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModelFS : DbContext
    {
        public ModelFS()
            : base("name=ModelFS")
        {
        }

        public virtual DbSet<Form> Forms { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
