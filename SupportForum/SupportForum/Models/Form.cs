namespace SupportForum.Models
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Form")]
    public partial class Form
    {
        public int Id { get; set; }

        [StringLength(50)]
        [DisplayName("To: ")]
        [Required(ErrorMessage = "Select a department")]
        public string To { get; set; }

        [StringLength(50)]
        [DisplayName("From: ")]
        [Required(ErrorMessage = "Type your email address")]
        public string From { get; set; }
       

        [StringLength(50)]
        [DisplayName("Subject: ")]
        [Required(ErrorMessage = "Enter a subject")]
        public string Subject { get; set; }

        [StringLength(50)]
        [DisplayName("Message: ")]
        [Required(ErrorMessage = "Please enter your message")]
        public string Message { get; set; }

      
    }

}
